var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify'),
    KeypadButton = require('./keypadButton');
   


var Keypad = React.createClass({
    
    getDefaultProps:function()
    {
        return{buttons:["1","2","3","4","5","6","7","8","9","0","X"]}
    },
  render: function() {
      
      var buttons =  this.props.buttons.map(function (button) {
              return (
                 <KeypadButton key={button} value = {button}/>
              );
            });
      
    return (
      <div className="keypad"> {buttons}</div>

    );
  },
    
    componentDidMount:function(){
      //  console.log("cell mounted");
        
   
    },
    
    componentWillUnmount:function(){
       // console.log("cell unmounted");
    }
});


module.exports = Keypad;