var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify');
   


var KeypadButton = React.createClass({
  render: function() {
    return (
      <div className="keypad-button" data-value={this.props.value} onClick={this.onKeyPress}>{this.props.value}<span></span></div>
    );
  },
    
     onKeyPress:function(e){

        //console.log('press',$(e.currentTarget).data('value'));

         Flux.doAction({actionType: 'keyPress', value: $(e.currentTarget).data('value')});
    },
     
    componentDidMount:function(){
        //console.log("button mounted");
    
    },
    
    componentWillUnmount:function(){
       // console.log("button unmounted");
    }
});


module.exports = KeypadButton;