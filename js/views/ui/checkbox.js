var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify');



var Checkbox = React.createClass({

    getInitialState:function(){

        return{isChecked:true}
    },


    isChecked: function() {
      return this.state.isChecked;
    },

    showError:function(){
        $(React.findDOMNode(this.refs.box)).css('background-color','red');
    },


  render: function()
  {
      var checked;
      if(this.state.isChecked)
      {
          checked = "x"
      }else
      {
          checked = ""
      }
    return (
      <div className="checkbox" id={this.props.id} onClick={this.onCheckClicked}>
          <div className="box" ref="box" >{checked}</div>
          <div className="label">{this.props.label}<a className="terms-text"> {this.props.labelTs}</a></div>
        </div>
    );
  },

     onCheckClicked:function()
    {
        $(document).trigger('someClick');
        if(this.state.isChecked == true)
        {
            this.setState({isChecked:false});
        }
        else
        {
            this.setState({isChecked:true});
             $(React.findDOMNode(this.refs.box)).css('background-color','white')
        }


    },

    componentDidMount:function(){


    },

    componentWillUnmount:function(){

    }
});


module.exports = Checkbox;
