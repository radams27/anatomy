var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react');



var Terms = React.createClass({
  render: function() {
    return (
      <div className="terms-container" ref="container" >
          <div className="terms-title">terms <br></br>and conditions</div>
        <div className="ok-btn" onClick={this.onOKClick}>OK</div>
        <div className="terms-content-1" ref="terms1">
            <h2>Terms and Conditions for the TFG Rewards & More Programme ("Rewards"):</h2>
            <ul className="left1">
                <li>The Rewards & More Programme ("Rewards") is a programme that provides certain benefits to customers of The Foschini Group ("TFG").</li>
                <li>Rewards is offered (or will be offered) in selected TFG stores in various jurisdictions in Africa, and is managed by the following companies: </li>
                <li>South Africa - Foschini retail Group (Pty) Ltd, registration No. 1988/007302/07</li>
                <li>Namibia  - Fashion Retailers (Pty) Ltd, Registration No. 821</li>
                <li>Botswana  - Foschini (Botswana) (Pty) Ltd, Registration No.811/1970</li>
                <li>Lesotho Foschini (Lesotho), Registration No.1980/56 in Lesotho</li>
                <li>Swaziland - Foschini (Swaziland), Registration No. 117/1980</li>
                <li>Zambia – Fashion retails (Zambia) Limited – registration No.</li>
                <li>Nigeria -  Foschini Nigeria Limited, Registration No.</li>
                <li>Ghana</li>
                <li>Kenya.</li>
                <li>To participate in Rewards and become a member, you will be required to provide us with all requested relevant information.</li>
                <li>Once you are a Rewards member, you will receive a promotional voucher (“Voucher”) in the form of a special slip printed at the till every time that you make a cash or account purchase in a TFG store, as long as your Rewards card or TFG account card is swiped at the till.</li>
                <li>Vouchers may only be redeemed in country of issue.</li>
                <li>You will not receive a Voucher:</li>
                <li>when a store is off-line. In such an event we will send you an SMS voucher when the store is on-line again (providing that we have  a valid cellphone number for you), or you can view your Voucher at the till point when you are next in-store; or</li>
                   <li>if you have already received a Voucher from a specific TFG brand in the last 24 hours; or</li>
                <li>if you are purchasing on lay-by.</li>

            </ul>
                <ul className="right1">

                    <li>If you make purchases on TFG online (if available), you will need to enter your unique Rewards number in order to receive a Voucher.</li>
                <li>You may not redeem a Voucher when purchasing the following goods, unless specifically indicated on your till slip:</li>
                <li>marked down or discounted items;</li>
                <li>TFG gift cards; or</li>
                <li>online merchandise (if available).</li>
                <li>You may not redeem a Voucher for service items (e.g. repairs).</li>
                <li>You will not be able to redeem a Voucher in the unlikely event that a store is off-line. In such an event, you will be able to redeem your Voucher when you are next in store.</li>
                 <li>Vouchers will be earned at the following TFG stores (not all stores trade in each country): @home, @home livingspace, American Swiss, Charles & Keith, Donna Claire, Duesouth, Exact!, Fabiani, Fashion Express, Foschini, G-Star Raw, Hi, , Markham, Mat & May, Sportscene, Sterns, and Totalsports, Sodabloc. We reserve the right to add or remove any TFG store/s participating in Rewards at our sole discretion.</li>
                 <li>If you are student between the ages of 18 – 24, you will automatically qualify for a 10% (ten percent) discount on full priced merchandise at all Sportscene stores in South Africa only. You must present a valid student card in order to qualify for this discount.</li>
                  <li>Vouchers will be issued at our sole discretion. We do not under any circumstances guarantee what Voucher you will receive, and under no circumstances may you request a different Voucher, or obtain a second Voucher at the time of purchase.</li>
                    <li>We reserve the right to cancel Rewards at any time. In such an event, we will honour all valid, non-expired Vouchers.</li>
                </ul>
                 <div className="right-btn" onClick={this.onNextClick}></div>
            </div>

            <div className="terms-content-2" ref="terms2">
                 <ul className="left1">


                     <li>For cash sales, Vouchers may only be redeemed by the person they were issued to, who must ensure that their as long as their Rewards card is presented and swiped in-store.</li>
                     <li>Lost or stolen Rewards cards can be replaced in-store for a fee. The cost for a replacement card must be paid in full before a new card can be issued and further Rewards can be earned or redeemed.</li>
                     <li>A Voucher is not refundable if you return the product that was purchased with the Reward. However, if you want to exchange an item, the store manager may use his/her discretion to allow you to do so without losing the benefit of your Voucher.</li>
                     <li>Only one Voucher may be redeemed per purchase. You may not accumulate Vouchers or use multiple Vouchers for one purchase.</li>
                     <li>A Voucher may only be used once.</li>
                     <li>All Vouchers have expiry dates what The expiry date on a Voucher will not be extended under any circumstances. If you do not use the Voucher before the expiry date you will lose the benefit of the Voucher.</li>
                     <li>Lost or stolen Vouchers may be replaced at any TFG store, or alternatively you can contact TFG customer services on +27 0860 576 575 / +27 (021) 938 7666 to replace a Voucher (in South Africa dial 0860 576 575 / 021 938 7666).</li>
                     <li>Lost Vouchers for free gifts and services, or for external suppliers outside of TFG cannot be replaced.</li>
                      <li>Your personal information:</li>
                         <li>Is collected for the purpose of administering the rewards Programme, and will only be shared with companies outside of TFG as set out below.</li>
                         <li>If you are a cash customer or a TFG account customers who has supplied us with your contact details, and you have given us permission to contact you for marketing and communication purposes, you agree that we may send you relevant marketing information and promotional offers from Rewards & More, TFG and our relevant business partners. This applies, but is not limited to, marketing goods and services to you across all marketing channels, including online /e-mail / mail / SMS / MMS / telephonic / social media. We will always give you an option to opt out of future
marketing whenever we market to you. Furthermore, we will never give your details to any company that is not part of TFG unless we are marketing to you via a third party.</li>
                </ul>

                     <ul className="right1">


                    <li>The personal information that we obtain or collect from you will always be treated as confidential and will not be shared with third parties unless you have permitted us to do so, or unless we market to you via a third party, in which event we will hold them to strict confidentiality requirements. We will only disclose your personal information to third parties that we do not contract with if we are required to do so by law.</li>
                    <li>We may use your personal information to conduct a credit bureau check for the potential purpose of offering you a TFG Account Card if you do not already have one.</li>
                    <li>We may use your purchasing information in conjunction with information about you provided by third parties, for market and product analysis, for the purpose of generating statistical reports for internal and external use. These reports will not contain any personal information that identifies you.</li>
                    <li>By agreeing to the terms of these terms and Conditions, you specifically authorise the disclosures set out above in 23)c and 23)d.</li>
                    <li>You will only receive reminder SMS’s when your Voucher is about to expire, if you have supplied us with your correct cellphone number and given us permission to SMS you.</li>
                    <li>It is your responsibility to ensure that your personal and contact details are correct and up todate to enable us to offer you Rewards offers and promotions. If your contact details change at any time, please contact TFG Customer Services (see 18 above). We will not be held
responsible if you do not receive your personalised offers in the event that your personal details have changed and you have not notified us.</li>
                        <li>Rewards may only be used in accordance with these Terms and Conditions.</li>
                        <li>These Terms and Conditions may be updated from time to time. Please visit our website at www.tfg.co.za for the latest update. You will be bound by any new Terms and Conditions.</li>
                    </ul>

                <div className="left-btn" onClick={this.onPrevClick}></div>
            </div>

        </div>
    );
  },
    onOKClick:function(){
        $(document).trigger('someClick');
        $(React.findDOMNode(this.refs.container)).hide();
    },

    onPrevClick:function(){
      $(React.findDOMNode(this.refs.terms1)).show();
      $(React.findDOMNode(this.refs.terms2)).hide();
    },

    onNextClick:function(){

        $(React.findDOMNode(this.refs.terms1)).hide();
        $(React.findDOMNode(this.refs.terms2)).show();
    },

    componentDidMount:function(){

          $(React.findDOMNode(this.refs.terms2)).hide();

    },

    componentWillUnmount:function(){

    }
});


module.exports = Terms;
