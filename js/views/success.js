var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react');
   

var timeout;

var Success = React.createClass({
  render: function() {
    return (
      <div className="success-bg" >
          <div className="success-title">Welcome</div>
        </div>
    );
  },
    
    
    componentDidMount:function(){

        timeout =  setTimeout(function(){
        
            Backbone.history.navigate('/',{trigger:true})
        },8000);
    },
    
    componentWillUnmount:function(){
       clearTimeout(timeout);
    }
});


module.exports = Success;