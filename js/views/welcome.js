var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify')
   


var Welcome = React.createClass({
  render: function() {
    return (
      <div className="welcome-bg" onClick={this.onScreenClick}>
          <div className="welcome-title"></div>
          <div className="welcome-cta">tap to begin</div>
          <div className="logo"></div>
        </div>
    );
  },
    
    onScreenClick:function(){
        Backbone.history.navigate('enter_cell', {trigger: true})
    },
    
    componentDidMount:function(){
        //console.log("mounted");
        Flux.doAction('updateCell',"");
    
    },
    
    componentWillUnmount:function(){
        //console.log("welcome unmounted");
    }
});


module.exports = Welcome;