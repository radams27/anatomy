var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify'),
    Keypad = require('./ui/keypad'),
    Checkbox = require('./ui/checkbox'),
    Store = require('../../stores/Store')
   


var EnterCode = React.createClass({
    
    
getInitialState: function() {
    return {value: ''};
  },
    
getDefaultProps:function(){

return {dispatcherID:null}
},
    
  render: function() {
      
      var label ="I have read the T's & C's";
      var label2 ="I'm happy to receive marketing material like sneaker drops,events and news";
      var c1 ="c1";
      var c2 ="c2";
    return (
      <div className="enter-cell-container" >
          <div className="keypad-container">
            <div className="heading-container">
                <h1>enter code</h1>
            </div>
                <Keypad onClick={this.onKeyPress}/>
            </div>
          <div className="right-container">
                <div className="number-input" ref="codeInput">{this.state.value}</div>
                <div className="proceed-btn" onClick={this.onProceedClick}>Proceed</div>
                <div className="cancel-btn" onClick={this.onCancelClick}>Cancel</div>
               
            </div>
        </div>
    );
  },
    
    onProceedClick:function()
    {
     
       
       var that = this;
       console.log("cell: " + Store.cell, "code: " + that.state.value);
       
        if(that.state.value.length > 0)
        {
            $.ajax({
                      method: "GET",
                       dataType: 'json',
                      url: "http://localhost/checkcode.php",
                      data: { 'cellno':Store.cell, 'code':  that.state.value},
                       success:function(data) {
                           
                           if(data.response == true)
                           {
                                Backbone.history.navigate('/success',{trigger:true})
                           }else{
                                Backbone.history.navigate("/failed",{trigger:true});
                           }
                        },
                       error:function(data){
                            console.log("error",data);
                             Backbone.history.navigate("/failed",{trigger:true});
                       }
                    })
        }
        else
        {
             $(React.findDOMNode(that.refs.codeInput)).css('border-bottom','4px solid red');
        }
                  
    },
    
     onCancelClick:function()
    {
       Backbone.history.navigate('/',{trigger:true})
    },
    
    componentDidMount:function()
    {
       // console.log("cell mounted");
        
        var that = this;
       var dispatcherID =  Flux.dispatcher.register( function( payload ){
           $(React.findDOMNode(that.refs.codeInput)).css('border-bottom','4px solid white');
        
            if(payload.actionType == "keyPress")
            {
                 var number;
                if(payload.value != "X")
                {
                    number = that.state.value + payload.value;
                   
                }
                else
                {
                     number = that.state.value.substr(0, that.state.value.length -1);
                }
                that.setState({value: number.substr(0, 10)});
            }
          
        });

        this.setProps({dispatcherID:dispatcherID});
    // console.log('id',id);
    },
    
    componentWillUnmount:function(){
        //console.log("cell unmounted");
        Flux.dispatcher.unregister(this.props.dispatcherID);
    }
});


module.exports = EnterCode;