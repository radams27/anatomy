var Backbone = require('backbone'),
    $ = require('jquery'),
    React = require('react'),
    Flux = require('fluxify'),
    Keypad = require('./ui/keypad'),
    Checkbox = require('./ui/checkbox'),
    Terms = require('./ui/terms'),
    Store = require('../../stores/Store')



var EnterCell = React.createClass({

  timeout: undefined,
  getInitialState: function() {
    return {
        value: '',
        dispatcherID:null
    };
  },

    getDefaultProps:function(){

    return {dispatcherID:null}
    },


  render: function() {

      var label ="I have read the";
      var labelTCS ="T's & C's";
      var label2 ="I'm happy to receive marketing material like sneaker drops,events and news";
      var c1 ="c1";
      var c2 ="c2";
    return (
      <div className="enter-cell-container" >
          <div className="keypad-container">
            <div className="heading-container">
                <h1><span className="l1">please enter</span><span className="l2"> mobile number</span></h1>
            </div>
                <Keypad onClick={this.onKeyPress}/>
            </div>
          <div className="right-container">
                <div className="number-input" ref="numberInput">{this.state.value}</div>
                <div className="send-number-btn" onClick={this.onSendClick}>Send Code</div>
                <div className="terms-button" onClick={this.onTermsClick}></div>
                <Checkbox key="terms" ref="terms" id={c1} label={label} labelTs ={labelTCS}/>
                <Checkbox key="marketing" ref="marketing" id={c2}label={label2} />
            </div>
            <Terms ref="termsContainer"/>
        </div>
    );
  },


    onTermsClick:function(){
        clearTimeout(this.timeout);
        $(React.findDOMNode(this.refs.termsContainer)).show();
    },

    onSendClick:function()
    {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
             Backbone.history.navigate("/",{trigger:true});
        }, 10000);
       if(this.refs.terms.isChecked())
       {
           if(this.state.value.length == 10)
           {

               var that = this;
               console.log("cellno: " + that.state.value, "marketing: " + this.refs.marketing.isChecked());

               Flux.doAction('updateCell',that.state.value);


           }
           else
           {
               $(React.findDOMNode(this.refs.numberInput)).css('border-bottom','4px solid red');
           }
       }
        else{
            this.refs.terms.showError();

        }
    },


    componentDidMount:function()
    {
        var that = this;
        $(document).on('someClick', function () {
            clearTimeout(that.timeout);
            that.timeout = setTimeout(function () {
                 Backbone.history.navigate("/",{trigger:true});
            }, 10000);
        });
        //console.log("cell mounted");
        this.timeout = setTimeout(function () {
             Backbone.history.navigate("/",{trigger:true});
        }, 10000);
        this.refs.terms.setState({isChecked:false});
        $(React.findDOMNode(this.refs.termsContainer)).hide();


        var dispatcherID = Flux.dispatcher.register( function( payload ){

            if(payload.actionType == "keyPress")
            {
                clearTimeout(that.timeout);
                that.timeout = setTimeout(function () {
                     Backbone.history.navigate("/",{trigger:true});
                }, 10000);
                $(React.findDOMNode(that.refs.numberInput)).css('border-bottom','4px solid white');
                 var number;
                if(payload.value != "X")
                {
                    number = that.state.value + payload.value;

                }
                else
                {
                     number = that.state.value.substr(0, that.state.value.length -1);
                }
                that.setState({value: number.substr(0, 10)});
            }

        });

        var that = this;
        var isChecked = that.refs.marketing.isChecked();
       // console.log("check marketing",that.refs.marketing)

        Store.on('change:cell',function( value, previousValue ){
            clearTimeout(that.timeout);
              $.ajax({
                  method: "GET",
                   dataType: 'json',
                  url: "http://localhost/start.php",
                  data: { 'cellno':that.state.value, 'marketing': isChecked },
                   success:function(data) {
                     Backbone.history.navigate("/enter_code",{trigger:true});
                    },
                   error:function(data){
                        console.log("error",data);
                       //Backbone.history.navigate("/failed",{trigger:true});

                       Backbone.history.navigate("/failed",{trigger:true});
                   }
                })

        });

       // console.log('id',test);

        this.setProps({dispatcherID:dispatcherID});

    },

    componentWillUnmount:function(){
        $(document).off('someClick');
         //.log("cell unmounted");
         Store.off('change:cell');
         Flux.dispatcher.unregister(this.props.dispatcherID);
    }
});


module.exports = EnterCell;
