var Flux = require('fluxify')

var Store = Flux.createStore({
    id: 'numberStore',
    initialState: {
        cell: ''
    },
    actionCallbacks: {
        updateCell: function( updater, cell ){
          console.log('update cell',cell);
            
            updater.set( {cell: cell} );
        }
    }
});

module.exports = Store;