var $ = require('jquery')(window);
var Backbone = require('backbone');
var Flux = require('fluxify'),
    Promise = require('es6-promise').Promise;
Backbone.$ = $;

var Router = require('./router');

Flux.promisify( Promise );
var router = new Router();



Backbone.history.start();
