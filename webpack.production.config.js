
var path = require("path");
var webpack = require('webpack');

module.exports = {
    context:path.resolve(""),
    entry: {
        bundle: [
            './app'
        ]
    },
    output:{
        path:path.resolve("public"),
        publicPath: '/',
        filename:"bundle.js"
    },

    module:{

        loaders:[
            {
                test:/\.js$/,
                exclude:/node_modules/,
                loaders:  ['react-hot', 'babel']
            },
            {
                test: /\.json$/,
                include: path.join(__dirname, 'node_modules'),
                loader: 'json-loader'
            },
            {
                test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'
            }
        ]
    },

    plugins: [
         new webpack.DefinePlugin({
          'process.env':{
            'NODE_ENV': JSON.stringify('production')
          }
        }),
         new webpack.optimize.UglifyJsPlugin({
          compress:{
            warnings: true
          }
        })
  ],

    resolve:{
        extensions:["",".js",".es6"],
        // alias: {
        //     'react': 'react-lite',
        // }

    }
}
