var $ = require('jquery'),
    Backbone = require('backbone'),
    Welcome = require('./js/views/welcome'),
    EnterCell = require('./js/views/enterCell'),
    EnterCode = require('./js/views/enterCode'),
    Success = require('./js/views/success'),
    Failed = require('./js/views/failed'),
    React = require('react');

Backbone.$ = $;



module.exports = Backbone.Router.extend({

    routes: {
        "": "home",
        "enter_cell":"enterCell",
        "enter_code":"enterCode",
        "success":"success",
        "failed":"failed"
        //"employees/:id": "employeeDetails",
       // "employees/:id/reports": "reports"

    },

    home: function () {
        console.log("home");

        
        React.render(
          <Welcome />,
          document.getElementById('container')
        );

    },
    
    enterCell:function()
    {
         React.render(
          <EnterCell />,
          document.getElementById('container')
        );
    },
    
    enterCode:function()
    {
         React.render(
          <EnterCode />,
          document.getElementById('container')
        );
    },
    
    success:function()
    {
        React.render(
          <Success />,
          document.getElementById('container')
        );

    },
    
     failed:function()
    {
        React.render(
          <Failed />,
          document.getElementById('container')
        );

    }

});