
var path = require("path");

function getEntrySources(sources) {
    if (process.env.NODE_ENV !== 'production') {
       // sources.push('webpack-dev-server/client?http://localhost:8080');
        //sources.push('webpack-dev-server/client?http://10.0.0.5:5000');
        //sources.push('webpack-dev-server/client?http://localhost:8080/share');
    }

    return sources;
}


module.exports = {
    context:path.resolve(""),
    entry: {
        bundle: getEntrySources([
            './app.js'
        ])
    },
    output:{
        path:path.resolve("public"),
        filename:"bundle.js"
    },
    watch:true,

    devServer:{
        contentBase:"public",
        inline:true,
        hot:true
        // host: "10.0.0.5",
        // port: 5000
    },

    node: { fs: "empty" },
    devtool:"inline-source-map",

    module:{

        loaders:[
            {
                test:/\.js$/,
                exclude:/node_modules/,
                loaders:  ['react-hot', 'babel']
            },
            {
                test: /\.json$/,
                include: path.join(__dirname, 'node_modules'),
                loader: 'json-loader'
            },
            {
                test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'
            }
        ]
    },

    resolve:{
        extensions:["",".js",".es6"]

    }


}
