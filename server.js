require('node-jsx').install()

var express = require('express')
  , app = express()
  , React = require('react/addons')
  , welcome = require('./js/views/Welcome')
  , enterCell = require('./js/views/EnterCell')
  , enterCode = require('./js/views/EnterCode')
  , success = require('./js/views/Success')
  , failed = require('./js/views/Failed')

var Welcome = React.createFactory(welcome)
var EnterCell = React.createFactory(enterCell)
var EnterCode = React.createFactory(enterCode)
var Success = React.createFactory(success)
var Failed = React.createFactory(failed)


app.engine('jade', require('jade').__express)
app.set('view engine', 'jade')
//app.set('view engine', 'ejs')

app.use(express.static('c:/work/anatomy/public'))

app.get('/', function(req, res){
 
    var reactHtml = React.renderToString(Welcome())
  res.render('index.ejs',{reactOutput:reactHtml});
});

app.get('/enter_cell', function(req, res){
 
    var reactHtml = React.renderToString(EnterCell())
  res.render('index.ejs',{reactOutput:reactHtml});
})



app.get('/enter_code', function(req, res){
 
    var reactHtml = React.renderToString(EnterCode())
  res.render('index.ejs',{reactOutput:reactHtml});
});

app.get('/success', function(req, res){
 
    var reactHtml = React.renderToString(Success())
  res.render('index.ejs',{reactOutput:reactHtml});
})

app.get('/failed', function(req, res){
 
    var reactHtml = React.renderToString(Failed())
  res.render('index.ejs',{reactOutput:reactHtml});
})

app.listen(3000, function() {
  console.log('Listening on port 3000...')
})